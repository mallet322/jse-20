package com.nlmk.jse;

public abstract class Shape {

    public abstract double getArea();

}
