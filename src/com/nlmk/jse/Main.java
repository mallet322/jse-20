package com.nlmk.jse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class Main {

    public static void main(String[] args) {

        Circle circle = new Circle(3);
        Circle circle1 = new Circle(5);
        Rectangle rectangle = new Rectangle(2, 5);
        Rectangle rectangle1 = new Rectangle(2, 5);
        Square square = new Square(4);
        Square square1 = new Square(4);

        Collection<Circle> circles = new ArrayList<>();
        addToFigure(circles, circle);
        addToFigure(circles, circle1);
        getSummaryArea(circles);

        Collection<Rectangle> rectangles = new ArrayList<>();
        addToFigure(rectangles, rectangle);
        addToFigure(rectangles, rectangle1);
        getSummaryArea(rectangles);

        Collection<Square> squares = new ArrayList<>();
        addToFigure(squares, square);
        addToFigure(squares, square1);
        getSummaryArea(squares);

        Collection<Shape> shapes = new ArrayList<>();
        addToFigure(shapes, circle);
        addToFigure(shapes, rectangle);
        addToFigure(shapes, square);
        getSummaryArea(shapes);

    }

    @SafeVarargs
    public static <T extends Shape> void addToFigure(Collection<T> collection, T... shapes) {
        collection.addAll(Arrays.asList(shapes));
    }

    public static void getSummaryArea(Collection<? extends Shape> collection) {
        double sum = 0;

        for (Shape shape : collection) {
            sum += shape.getArea();
        }
        System.out.println("Summary area is: " + sum);
    }


}