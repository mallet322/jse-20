package com.nlmk.jse;

public class Circle extends Shape {

    private final double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    static double square(double radius) {
        return radius * radius;
    }

    @Override
    public double getArea() {
        return Math.PI * square(radius);
    }

}