package com.nlmk.jse;

public class Square extends Shape {

    private final double length;

    public Square(double length) {
        this.length = length;
    }


    static double square(double length) {
        return length * length;
    }

    @Override
    public double getArea() {
        return square(length);
    }

}